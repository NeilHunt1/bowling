package com.nhunt.bowling;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nhunt.bowling.components.Game;

public class DatabaseHandler{
	//static final long serialVersionUID = 1L;
	final Logger logger = LoggerFactory.getLogger(DatabaseHandler.class);
	String jdbcUrl;
	String username;
	String password; 
	Connection conn;
	
	public DatabaseHandler(){
		try {
			//AppFog deployment
			if(System.getenv("VCAP_SERVICES")!=null){
				String vcapVar = System.getenv("VCAP_SERVICES");
				logger.debug("VCAP_SERVICES set to "+vcapVar);
				JSONParser parser = new JSONParser();
				try {
					JSONObject jsonObj = (JSONObject)parser.parse(vcapVar);
					JSONArray name = (JSONArray) jsonObj.get("postgresql-9.1");
					Iterator<JSONObject> iterator = name.iterator();
					while (iterator.hasNext()) {
						JSONObject arrayItem = iterator.next();
						JSONObject credentialsItem;
						if(arrayItem.containsKey("credentials")){
							credentialsItem = (JSONObject)arrayItem.get("credentials");
							username = (String)credentialsItem.get("username");
							password = (String)((JSONObject)arrayItem.get("credentials")).get("password");
							String host = (String)((JSONObject)arrayItem.get("credentials")).get("host");
							Long port = (Long)(((JSONObject)arrayItem.get("credentials")).get("port"));
							String dbName = (String)((JSONObject)arrayItem.get("credentials")).get("name");
							jdbcUrl = new StringBuilder().append("jdbc:postgresql://").append(host).append(":").append(port).append("/").append(dbName).toString();
						}
						else{
							logger.debug("didn't find credentials item, but kind of think we should have.");
						}
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			//OpenShift deployment
			else if(System.getenv("OPENSHIFT_POSTGRESQL_DB_URL")!=null){
				logger.info("OPENSHIFT_POSTGRESQL_DB_URL is set, we should be running on OPENSHIFT");
				if(System.getenv("OPENSHIFT_POSTGRESQL_DB_HOST")!=null){
					jdbcUrl="jdbc:postgresql://"+System.getenv("OPENSHIFT_POSTGRESQL_DB_HOST")+":"+System.getenv("OPENSHIFT_POSTGRESQL_DB_PORT")+"/bowling";
				} 
				if(System.getenv("OPENSHIFT_POSTGRESQL_DB_USERNAME")!=null){
					username=System.getenv("OPENSHIFT_POSTGRESQL_DB_USERNAME");
				}
				if(System.getenv("OPENSHIFT_POSTGRESQL_DB_PASSWORD")!=null){
					password=System.getenv("OPENSHIFT_POSTGRESQL_DB_PASSWORD");
				}
			} 
			//generic/local deployment, uses properties file
			else {
				logger.info("Neither VCAP nor OPENSHIFT variables set");
				System.out.println("SysOut - Neither VCAP nor OPENSHIFT variables set");
				Properties dbprops = new Properties();
				dbprops.load(this.getClass().getClassLoader().getResourceAsStream ("liquibase.properties"));
				jdbcUrl=dbprops.getProperty("url");
				username=dbprops.getProperty("username");
				password=dbprops.getProperty("password");
				logger.info("no env variable not set");
			}
			logger.debug("vars are: jdbcUrl: "+jdbcUrl+", username: "+username+", password: "+password);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getDBConnection() throws SQLException{
		//conn = null;
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("jdbcurl: "+jdbcUrl+", username: "+username+", password: "+password);
		conn = DriverManager.getConnection(jdbcUrl,username,password);
	}
	public Connection getConn(){
		return conn;
	}
}