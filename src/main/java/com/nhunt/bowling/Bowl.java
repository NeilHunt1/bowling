package com.nhunt.bowling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nhunt.bowling.components.Game;

public class Bowl {
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Game g = new Game(args[0]);
		Logger logger = LoggerFactory.getLogger(Bowl.class);
		logger.info("FINAL SCORE: "+g.calculateFinalScore());
	}
	

}
