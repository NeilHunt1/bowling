package com.nhunt.bowling.components;

import java.io.Serializable;

public class BowlingResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String gameResults;
	private String finalScore;
	public BowlingResult(String gameResults,String finalScore){
		this.finalScore=finalScore;
		this.gameResults=gameResults;
	}
	public String getGameResults() {
		return gameResults;
	}
	public void setGameResults(String gameResults) {
		this.gameResults = gameResults;
	}
	public String getFinalScore() {
		return finalScore;
	}
	public void setFinalScore(String finalScore) {
		this.finalScore = finalScore;
	}
	
}
