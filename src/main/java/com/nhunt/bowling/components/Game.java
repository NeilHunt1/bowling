package com.nhunt.bowling.components;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.wicket.serialize.ISerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Game implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final Logger logger = LoggerFactory.getLogger(Game.class);

	private String gameString;
	private ArrayList<Frame> gameFrames;
	private int finalScore;
	
	public Game(){
	}
	
	public Game (String gameString){
		this.gameString = gameString;
	}
	
	/**
	 * parse the game String and process each frame accordingly
	 */
	public void playGame(){
		finalScore = 0;
		gameFrames = new ArrayList<Frame>();
		String [] frames = gameString.split("-");
		//TODO: to consider down the road, verify that there are ten frames and throw an exception if not
		for (int frameNumber = 0; frameNumber<10; frameNumber++){
			if(frames[frameNumber].equals("X") && (frames[frameNumber+1].equals("X") || frames[frameNumber+1].equals("XX"))){
				processFrame(frames[frameNumber], frames[frameNumber+1], frames[frameNumber+2]);
			}
			else if (frames[frameNumber].endsWith("/") || frames[frameNumber].equals("X")){
				processFrame(frames[frameNumber], frames[frameNumber+1], null);
			}
			else{
				processFrame(frames[frameNumber], null, null);
			}
		}
		
	}
	/**
	 * 
	 * @param frameScore
	 * 
	 */
	public void processFrame(String currentFrame, String nextFrame, String stringAfterNext){
		Frame f = new Frame();
		logger.debug("Current frame: {}",currentFrame);
		logger.debug("Next frame: {}",nextFrame);
		logger.debug("String after next: {}",stringAfterNext);
		if(currentFrame.equals("X")){
			f.processStrike(currentFrame,nextFrame,stringAfterNext);
		}
		else if(currentFrame.length()==2 && currentFrame.endsWith("/")){
			f.processBaseRolls(currentFrame.substring(0,1)+(10-Integer.parseInt(currentFrame.substring(0,1))));
			f.processSpare(currentFrame,nextFrame);
		}
		else if(currentFrame.length()==2){
			f.processBaseRolls(currentFrame);
		}
		else{
			//TODO: handle exception for improper input format down the road
		}
		logger.debug("this frame score is {}",f.calculateFrameScore());
		gameFrames.add(f);
	}
	public int calculateFinalScore(){
		for(Frame f : gameFrames){
			finalScore+=f.calculateFrameScore();
			logger.info("Final score: {}",finalScore);
		}
		return finalScore;
	}
	public int getFinalScore() {
		return finalScore;
	}
	/**
	 * resets game score
	 */
	public void resetGame(){
		finalScore=0;
	}

	public String getGameString() {
		return gameString;
	}

	public void setGameString(String gameString) {
		this.gameString = gameString;
	}
}
