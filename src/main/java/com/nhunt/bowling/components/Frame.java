package com.nhunt.bowling.components;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Frame implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Logger logger = LoggerFactory.getLogger(Frame.class);
	private int roll1;
	private int roll2;
	private int additional;
	
	public int getAdditional() {
		return additional;
	}
	public void setAdditional(int additional) {
		this.additional = additional;
	}
	public int getRoll1() {
		return roll1;
	}
	public void setRoll1(int frame1) {
		this.roll1 = frame1;
	}
	public int getRoll2() {
		return roll2;
	}
	public void setRoll2(int frame2) {
		this.roll2 = frame2;
	}
	public int calculateFrameScore(){
		int frameScore = 0;
		frameScore+=getRoll1();
		frameScore+=getRoll2();
		frameScore+=getAdditional();
		return frameScore;
	}
	/*
	 * scores a frame with a strike on the first roll
	 */
	public void processStrike(String currentFrame, String nextFrame, String frameAfterNext){
		setRoll1(10);
		if(nextFrame.equals("X")){
			if(frameAfterNext.substring(0,1).equals("X")){
				setAdditional(20);
			}
			else{
				setAdditional(10+Integer.parseInt(frameAfterNext.substring(0,1)));
			}
		}
		else if(nextFrame.equals("XX")){
			setAdditional(20);
		}
		else if(nextFrame.endsWith("/")){
			logger.debug("Spare after a strike");
			setAdditional(10);
		}
		else{
			setAdditional(Integer.parseInt(nextFrame.substring(0,1))+Integer.parseInt(nextFrame.substring(1,2)));
		}
	}
	/**
	 * scores a frame with a spare
	 * @param currentFrame
	 * @param nextFrame
	 */
	public void processSpare(String currentFrame, String nextFrame){
		if(nextFrame.equals("X")){
			setAdditional(10);
		}
		else{
			setAdditional(Integer.parseInt(nextFrame.substring(0,1)));
		}
	}
	/**
	 * processes the two rolls 
	 * @param currentFrame
	 */
	public void processBaseRolls(String currentFrame) {
		setRoll1(Integer.parseInt(currentFrame.substring(0,1)));
		setRoll2(Integer.parseInt(currentFrame.substring(1,2)));
	}
}
