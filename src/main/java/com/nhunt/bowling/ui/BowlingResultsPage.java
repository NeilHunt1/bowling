package com.nhunt.bowling.ui;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.nhunt.bowling.DatabaseHandler;
import com.nhunt.bowling.components.BowlingResult;

public class BowlingResultsPage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	public BowlingResultsPage(final PageParameters parameters) {
		Statement stmt;
		ResultSet rset;
		String query;
		DatabaseHandler dh = new DatabaseHandler();
		try {
			dh.getDBConnection();
			Connection conn = dh.getConn();
			stmt = conn.createStatement();
			query = "SELECT * FROM BOWLING_RESULTS";
			rset = stmt.executeQuery(query);
			List<BowlingResult> scoreList = new ArrayList<BowlingResult>();
			while (rset.next()) {
				scoreList.add(new BowlingResult(rset.getString("LINE_SCORE"),rset.getString("FINAL_SCORE")));
			}
			add(new PropertyListView<BowlingResult>("listview", scoreList) {
				private static final long serialVersionUID = -7037591062370775134L;
				@Override
				protected void populateItem(ListItem<BowlingResult> li) {
					li.add(new Label("gameResults"));
					li.add(new Label("finalScore"));
				}
			});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
