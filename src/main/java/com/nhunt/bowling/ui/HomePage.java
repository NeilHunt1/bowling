package com.nhunt.bowling.ui;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nhunt.bowling.DatabaseHandler;
import com.nhunt.bowling.components.Game;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	public HomePage(final PageParameters parameters) {
		final Logger logger = LoggerFactory.getLogger(HomePage.class);
		
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		add(feedback);

		final Game g = new Game();
    	Form<Game> bgf = new Form<Game>("bowlingGameForm",new CompoundPropertyModel<Game>(g));
    			
		final RequiredTextField<String> gameField = new RequiredTextField<String>("gameString");
		gameField.add(StringValidator.minimumLength(22));
		gameField.setOutputMarkupId(true);
		bgf.add(gameField);

		final Label gameResults = new Label("gameResults",new PropertyModel<Game>(g,"finalScore"));
		gameResults.setOutputMarkupId(true);
		gameResults.setVisible(true);
		add(gameResults);
		
		bgf.add(new AjaxButton("submitButton"){

			private static final long serialVersionUID = 1L;

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				logger.debug("this is debug message: "+feedback);
				logger.info("bowlstrike info: "+feedback);
				logger.warn("this is warn message: "+"warning!");
				target.add(feedback);
			}

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
		    	g.playGame();
		    	g.calculateFinalScore();
		    	gameResults.setVisible(true);
		    	target.add(gameResults);
				target.add(feedback);
				Statement stmt;
				String query;
				DatabaseHandler dh = new DatabaseHandler();
				try {
					dh.getDBConnection();
					Connection conn = dh.getConn();
					stmt = conn.createStatement();
					query = "INSERT INTO BOWLING_RESULTS VALUES ('" + g.getGameString()+"','"+g.getFinalScore()+"')";
					stmt.executeQuery(query);
				} catch(SQLException sqle){
					logger.warn(sqle.getSQLState());
				}
			}
			
		});
    	add(bgf);
    }
}
