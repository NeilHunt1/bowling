package com.nhunt.bowling.selenium;

import static junit.framework.Assert.assertEquals;

import java.net.URL;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple {@link RemoteWebDriver} test that demonstrates how to run your
 * Selenium tests with <a href="http://saucelabs.com/ondemand">Sauce
 * OnDemand</a>. *
 * 
 * @author Ross Rowe
 */
public class WebDriverTest {

	private ArrayList<DesiredCapabilities> capabilitiesList;
	final Logger logger = LoggerFactory.getLogger(WebDriverTest.class);
	@Before
	public void setUp() throws Exception {

		capabilitiesList = new ArrayList<DesiredCapabilities>();
		//TODO: fill this capabillities List with entries from Properties file or Jenkins environment variables
		DesiredCapabilities ffWin8 = DesiredCapabilities.firefox();
		ffWin8.setCapability("version", "22");
		ffWin8.setCapability("platform", Platform.WINDOWS);
		capabilitiesList.add(ffWin8);
		DesiredCapabilities chromeWin7 = DesiredCapabilities.chrome();
		chromeWin7.setCapability("version", "31");
		chromeWin7.setCapability("platform", Platform.WIN8);
		capabilitiesList.add(chromeWin7);
	}

	//@Test
	public void testPageLoading() throws Exception {
		
		for(DesiredCapabilities capabillities:capabilitiesList){
			logVariableInfo();
			WebDriver driver = new RemoteWebDriver(
					new URL(
							"http://neilhunt1:475d5ec0-19e8-4379-b660-c038d37eb5c3@ondemand.saucelabs.com:80/wd/hub"),
					capabillities);
			String baseUrl = "http://computechinc-test.eu01.aws.af.cm/";
			driver.get(baseUrl);
			assertEquals("Hunt Bowling League Powered by Apache Wicket",
					driver.getTitle());
			driver.navigate().to(baseUrl+"wicket/bookmarkable/com.nhunt.bowling.ui.BowlingResultsPage");
			assertEquals("Hunt Bowling League Results",driver.getTitle());
			driver.quit();
		}
	}
	
	private void logVariableInfo(){
		if(System.getProperty("SELENIUM_PLATFORM")!=null){
			logger.info("WORKING WITH SAUCE VARIABLES --- SELENIUM_PLATFORM: "+System.getProperty("SELENIUM_PLATFORM"));
			logger.info("WORKING WITH SAUCE VARIABLES --- SELENIUM_VERSION: "+System.getProperty("SELENIUM_VERSION"));
			logger.info("WORKING WITH SAUCE VARIABLES --- SELENIUM_BROWSER: "+System.getProperty("SELENIUM_BROWSER"));
			logger.info("WORKING WITH SAUCE VARIABLES --- SAUCE_ONDEMAND_BROWSERS: "+System.getProperty("SAUCE_ONDEMAND_BROWSERS"));
		}
	}

}
