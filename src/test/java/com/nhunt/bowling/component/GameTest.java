package com.nhunt.bowling.component;


import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nhunt.bowling.components.Game;

public class GameTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testValidInputString(){
		//test that it has exactly 10 frames, unless the last one is a spare or a strike.
		//if the last one is a spare, should have only one additional frame with one roll
		//if last one is a strike, should have one additional frame with two rolls
	}
	
	/**
	 * tests basic bowl with no spares or strikes
	 */
	@Test
	public void testBasicBowl() {
		Game g = new Game("17-02-32-10-72-41-52-14-44-27");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(59, score);
	}
	
	/**
	 * tests spare calculator, should give ten for each spare + the score of the first roll on the next frame
	 */
	@Test
	public void testMidGameSpares(){
		Game g = new Game("17-3/-32-10-72-4/-52-14-44-27");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(80, score);
	}
	
	/**
	 * tests having a spare on the tenth frame
	 */
	@Test
	public void testEndGameSpare(){
		Game g = new Game("17-02-32-10-72-41-52-14-44-2/-6");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(66, score);
	}
	
	/**
	 * tests strikes thrown within the game
	 */
	@Test
	public void testMidGameStrikes(){
		Game g = new Game("X-02-X-10-72-41-52-X-44-25");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(80, score);
	}
	
	/**
	 * tests strikes thrown within the game
	 */
	@Test
	public void testMidGameStrikesAndSparesTogether(){
		Game g = new Game("4/-X-X-10-72-41-52-X-44-25");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(107, score);
	}
	
	/**
	 * tests spare at the end of the game
	 */
	@Test
	public void testTenthFrameSpare(){
		Game g = new Game("17-02-32-10-72-41-52-14-44-2/-5");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(65, score);
	}
	
	/**
	 * tests strike at end of game
	 */
	@Test
	public void testTenthFrameStrike(){
		Game g = new Game("17-X-32-10-72-41-52-14-44-X-5/");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(83, score);
	}
	
	/**
	 * tests perfect game
	 */
	@Test
	public void testPerfectGame(){
		Game g = new Game("X-X-X-X-X-X-X-X-X-X-X-X");
		g.playGame();
		int score = g.calculateFinalScore();
		assertEquals(300, score);
	}
	
	//OTHER POSSIBLE TESTS if we went further to verify the input String. Would need to verify certain Exceptions were thrown
	//("17+02+32+10+72+41+52+14+44+2/") -- should fail since there's a spare at the end but no additional roll
	//("17+02+32+10+72+41+52+14+44+X+6") -- should fail since it's a strike but there's only one additional roll

}
